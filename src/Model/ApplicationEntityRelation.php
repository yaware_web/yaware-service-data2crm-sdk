<?php

namespace Data2CRMAPI\Model;

use \ArrayAccess;

class ApplicationEntityRelation extends AbstractModel  implements ArrayAccess
{   
    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'key' => 'string'
    );

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = array(
        'key' => 'key'
    );

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = array(
        'key' => 'setKey'
    );

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = array(
        'key' => 'getKey'
    );

    /**
     * Gets key
     *
     * @return string
     */
    public function getKey()
    {
        return $this->offsetGet('key');
    }

    /**
     * Sets key
     *
     * @param string $key Application key
     *
     * @return $this
     */
    public function setKey($key)
    {
        $this->offsetSet('key', $key);

        return $this;
    }
}
