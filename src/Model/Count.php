<?php

namespace Data2CRMAPI\Model;

use \ArrayAccess;

class Count extends AbstractModel  implements ArrayAccess
{   
    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'total' => 'int'
    );

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = array(
        'total' => 'total'
    );

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = array(
        'total' => 'setTotal'
    );

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = array(
        'total' => 'getTotal'
    );

    /**
     * Gets total
     *
     * @return int
     */
    public function getTotal()
    {
        return $this->offsetGet('total');
    }

    /**
     * Sets total
     *
     * @param int $total Total
     *
     * @return $this
     */
    public function setTotal($total)
    {
        $this->offsetSet('total', $total);

        return $this;
    }
}
