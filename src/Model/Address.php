<?php

namespace Data2CRMAPI\Model;

use \ArrayAccess;

class Address extends AbstractModel  implements ArrayAccess
{   
    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'type' => 'string',
        'street' => 'string',
        'city' => 'string',
        'state' => 'string',
        'country' => 'string',
        'zip' => 'string'
    );

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = array(
        'type' => 'type',
        'street' => 'street',
        'city' => 'city',
        'state' => 'state',
        'country' => 'country',
        'zip' => 'zip'
    );

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = array(
        'type' => 'setType',
        'street' => 'setStreet',
        'city' => 'setCity',
        'state' => 'setState',
        'country' => 'setCountry',
        'zip' => 'setZip'
    );

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = array(
        'type' => 'getType',
        'street' => 'getStreet',
        'city' => 'getCity',
        'state' => 'getState',
        'country' => 'getCountry',
        'zip' => 'getZip'
    );

    /**
     * Gets type
     *
     * @return string
     */
    public function getType()
    {
        return $this->offsetGet('type');
    }

    /**
     * Sets type
     *
     * @param string $type Type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->offsetSet('type', $type);

        return $this;
    }
    /**
     * Gets street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->offsetGet('street');
    }

    /**
     * Sets street
     *
     * @param string $street Street
     *
     * @return $this
     */
    public function setStreet($street)
    {
        $this->offsetSet('street', $street);

        return $this;
    }
    /**
     * Gets city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->offsetGet('city');
    }

    /**
     * Sets city
     *
     * @param string $city City
     *
     * @return $this
     */
    public function setCity($city)
    {
        $this->offsetSet('city', $city);

        return $this;
    }
    /**
     * Gets state
     *
     * @return string
     */
    public function getState()
    {
        return $this->offsetGet('state');
    }

    /**
     * Sets state
     *
     * @param string $state State
     *
     * @return $this
     */
    public function setState($state)
    {
        $this->offsetSet('state', $state);

        return $this;
    }
    /**
     * Gets country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->offsetGet('country');
    }

    /**
     * Sets country
     *
     * @param string $country Country
     *
     * @return $this
     */
    public function setCountry($country)
    {
        $this->offsetSet('country', $country);

        return $this;
    }
    /**
     * Gets zip
     *
     * @return string
     */
    public function getZip()
    {
        return $this->offsetGet('zip');
    }

    /**
     * Sets zip
     *
     * @param string $zip Zip
     *
     * @return $this
     */
    public function setZip($zip)
    {
        $this->offsetSet('zip', $zip);

        return $this;
    }
}
