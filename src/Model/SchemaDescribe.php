<?php

namespace Data2CRMAPI\Model;

use \ArrayAccess;

class SchemaDescribe extends AbstractModel  implements ArrayAccess
{   
    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'fetchAll' => '\Data2CRMAPI\Model\FetchAllDescribe',
        'fetch' => '\Data2CRMAPI\Model\FetchDescribe',
        'create' => '\Data2CRMAPI\Model\CreateDescribe',
        'update' => '\Data2CRMAPI\Model\UpdateDescribe'
    );

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = array(
        'fetchAll' => 'fetchAll',
        'fetch' => 'fetch',
        'create' => 'create',
        'update' => 'update'
    );

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = array(
        'fetchAll' => 'setFetchAll',
        'fetch' => 'setFetch',
        'create' => 'setCreate',
        'update' => 'setUpdate'
    );

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = array(
        'fetchAll' => 'getFetchAll',
        'fetch' => 'getFetch',
        'create' => 'getCreate',
        'update' => 'getUpdate'
    );

    /**
     * Gets fetchAll
     *
     * @return \Data2CRMAPI\Model\FetchAllDescribe
     */
    public function getFetchAll()
    {
        return $this->offsetGet('fetchAll');
    }

    /**
     * Sets fetchAll
     *
     * @param \Data2CRMAPI\Model\FetchAllDescribe $fetchAll Fetch all or a subset of resources body response
     *
     * @return $this
     */
    public function setFetchAll($fetchAll)
    {
        $this->offsetSet('fetchAll', $fetchAll);

        return $this;
    }
    /**
     * Gets fetch
     *
     * @return \Data2CRMAPI\Model\FetchDescribe
     */
    public function getFetch()
    {
        return $this->offsetGet('fetch');
    }

    /**
     * Sets fetch
     *
     * @param \Data2CRMAPI\Model\FetchDescribe $fetch Fetch a resource response body schema
     *
     * @return $this
     */
    public function setFetch($fetch)
    {
        $this->offsetSet('fetch', $fetch);

        return $this;
    }
    /**
     * Gets create
     *
     * @return \Data2CRMAPI\Model\CreateDescribe
     */
    public function getCreate()
    {
        return $this->offsetGet('create');
    }

    /**
     * Sets create
     *
     * @param \Data2CRMAPI\Model\CreateDescribe $create Create a resource request body schema
     *
     * @return $this
     */
    public function setCreate($create)
    {
        $this->offsetSet('create', $create);

        return $this;
    }
    /**
     * Gets update
     *
     * @return \Data2CRMAPI\Model\UpdateDescribe
     */
    public function getUpdate()
    {
        return $this->offsetGet('update');
    }

    /**
     * Sets update
     *
     * @param \Data2CRMAPI\Model\UpdateDescribe $update Update a resource request body schema
     *
     * @return $this
     */
    public function setUpdate($update)
    {
        $this->offsetSet('update', $update);

        return $this;
    }
}
