<?php

namespace Data2CRMAPI\Model;

use \ArrayAccess;

class ApplicationEntityWrite extends AbstractModel  implements ArrayAccess
{   
    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'type' => 'string',
        'authorization' => 'string',
        'credential' => '\Data2CRMAPI\Model\Credential[]',
        'description' => 'string'
    );

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = array(
        'type' => 'type',
        'authorization' => 'authorization',
        'credential' => 'credential',
        'description' => 'description'
    );

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = array(
        'type' => 'setType',
        'authorization' => 'setAuthorization',
        'credential' => 'setCredential',
        'description' => 'setDescription'
    );

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = array(
        'type' => 'getType',
        'authorization' => 'getAuthorization',
        'credential' => 'getCredential',
        'description' => 'getDescription'
    );

    /**
     * Gets type
     *
     * @return string
     */
    public function getType()
    {
        return $this->offsetGet('type');
    }

    /**
     * Sets type
     *
     * @param string $type Application platform type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->offsetSet('type', $type);

        return $this;
    }
    /**
     * Gets authorization
     *
     * @return string
     */
    public function getAuthorization()
    {
        return $this->offsetGet('authorization');
    }

    /**
     * Sets authorization
     *
     * @param string $authorization Application authorization
     *
     * @return $this
     */
    public function setAuthorization($authorization)
    {
        $this->offsetSet('authorization', $authorization);

        return $this;
    }
    /**
     * Gets credential
     *
     * @return \Data2CRMAPI\Model\Credential[]
     */
    public function getCredential()
    {
        return $this->offsetGet('credential');
    }

    /**
     * Sets credential
     *
     * @param \Data2CRMAPI\Model\Credential[] $credential Application credential
     *
     * @return $this
     */
    public function setCredential($credential)
    {
        $this->offsetSet('credential', $credential);

        return $this;
    }
    /**
     * Gets description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->offsetGet('description');
    }

    /**
     * Sets description
     *
     * @param string $description Application description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->offsetSet('description', $description);

        return $this;
    }
}
