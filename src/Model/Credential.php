<?php

namespace Data2CRMAPI\Model;

use \ArrayAccess;

class Credential extends AbstractModel  implements ArrayAccess
{   
    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'name' => 'string',
        'value' => 'string'
    );

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = array(
        'name' => 'name',
        'value' => 'value'
    );

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = array(
        'name' => 'setName',
        'value' => 'setValue'
    );

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = array(
        'name' => 'getName',
        'value' => 'getValue'
    );

    /**
     * Gets name
     *
     * @return string
     */
    public function getName()
    {
        return $this->offsetGet('name');
    }

    /**
     * Sets name
     *
     * @param string $name Name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->offsetSet('name', $name);

        return $this;
    }
    /**
     * Gets value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->offsetGet('value');
    }

    /**
     * Sets value
     *
     * @param string $value Value
     *
     * @return $this
     */
    public function setValue($value)
    {
        $this->offsetSet('value', $value);

        return $this;
    }
}
