<?php

namespace Data2CRMAPI\Model;

use \ArrayAccess;

class UserEntity extends AbstractModel  implements ArrayAccess
{   
    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'id' => 'string',
        'salutation' => 'string',
        'firstName' => 'string',
        'middleName' => 'string',
        'lastName' => 'string',
        'nameSuffix' => 'string',
        'username' => 'string',
        'description' => 'string',
        'position' => 'string',
        'department' => 'string',
        'status' => 'string',
        'isAdmin' => 'bool',
        'email' => '\Data2CRMAPI\Model\Email[]',
        'phone' => '\Data2CRMAPI\Model\Phone[]',
        'messenger' => '\Data2CRMAPI\Model\Messenger[]',
        'website' => '\Data2CRMAPI\Model\Website[]',
        'address' => '\Data2CRMAPI\Model\Address[]',
        'createdBy' => '\Data2CRMAPI\Model\UserEntityRelation',
        'createdAt' => '\DateTime',
        'updatedBy' => '\Data2CRMAPI\Model\UserEntityRelation',
        'updatedAt' => '\DateTime'
    );

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = array(
        'id' => 'id',
        'salutation' => 'salutation',
        'firstName' => 'first_name',
        'middleName' => 'middle_name',
        'lastName' => 'last_name',
        'nameSuffix' => 'name_suffix',
        'username' => 'username',
        'description' => 'description',
        'position' => 'position',
        'department' => 'department',
        'status' => 'status',
        'isAdmin' => 'is_admin',
        'email' => 'email',
        'phone' => 'phone',
        'messenger' => 'messenger',
        'website' => 'website',
        'address' => 'address',
        'createdBy' => 'created_by',
        'createdAt' => 'created_at',
        'updatedBy' => 'updated_by',
        'updatedAt' => 'updated_at'
    );

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = array(
        'id' => 'setId',
        'salutation' => 'setSalutation',
        'firstName' => 'setFirstName',
        'middleName' => 'setMiddleName',
        'lastName' => 'setLastName',
        'nameSuffix' => 'setNameSuffix',
        'username' => 'setUsername',
        'description' => 'setDescription',
        'position' => 'setPosition',
        'department' => 'setDepartment',
        'status' => 'setStatus',
        'isAdmin' => 'setIsAdmin',
        'email' => 'setEmail',
        'phone' => 'setPhone',
        'messenger' => 'setMessenger',
        'website' => 'setWebsite',
        'address' => 'setAddress',
        'createdBy' => 'setCreatedBy',
        'createdAt' => 'setCreatedAt',
        'updatedBy' => 'setUpdatedBy',
        'updatedAt' => 'setUpdatedAt'
    );

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = array(
        'id' => 'getId',
        'salutation' => 'getSalutation',
        'firstName' => 'getFirstName',
        'middleName' => 'getMiddleName',
        'lastName' => 'getLastName',
        'nameSuffix' => 'getNameSuffix',
        'username' => 'getUsername',
        'description' => 'getDescription',
        'position' => 'getPosition',
        'department' => 'getDepartment',
        'status' => 'getStatus',
        'isAdmin' => 'getIsAdmin',
        'email' => 'getEmail',
        'phone' => 'getPhone',
        'messenger' => 'getMessenger',
        'website' => 'getWebsite',
        'address' => 'getAddress',
        'createdBy' => 'getCreatedBy',
        'createdAt' => 'getCreatedAt',
        'updatedBy' => 'getUpdatedBy',
        'updatedAt' => 'getUpdatedAt'
    );

    /**
     * Gets id
     *
     * @return string
     */
    public function getId()
    {
        return $this->offsetGet('id');
    }

    /**
     * Sets id
     *
     * @param string $id User Identifier
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->offsetSet('id', $id);

        return $this;
    }
    /**
     * Gets salutation
     *
     * @return string
     */
    public function getSalutation()
    {
        return $this->offsetGet('salutation');
    }

    /**
     * Sets salutation
     *
     * @param string $salutation Salutation
     *
     * @return $this
     */
    public function setSalutation($salutation)
    {
        $this->offsetSet('salutation', $salutation);

        return $this;
    }
    /**
     * Gets firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->offsetGet('firstName');
    }

    /**
     * Sets firstName
     *
     * @param string $firstName First Name
     *
     * @return $this
     */
    public function setFirstName($firstName)
    {
        $this->offsetSet('firstName', $firstName);

        return $this;
    }
    /**
     * Gets middleName
     *
     * @return string
     */
    public function getMiddleName()
    {
        return $this->offsetGet('middleName');
    }

    /**
     * Sets middleName
     *
     * @param string $middleName Middle Name
     *
     * @return $this
     */
    public function setMiddleName($middleName)
    {
        $this->offsetSet('middleName', $middleName);

        return $this;
    }
    /**
     * Gets lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->offsetGet('lastName');
    }

    /**
     * Sets lastName
     *
     * @param string $lastName Last Name
     *
     * @return $this
     */
    public function setLastName($lastName)
    {
        $this->offsetSet('lastName', $lastName);

        return $this;
    }
    /**
     * Gets nameSuffix
     *
     * @return string
     */
    public function getNameSuffix()
    {
        return $this->offsetGet('nameSuffix');
    }

    /**
     * Sets nameSuffix
     *
     * @param string $nameSuffix Name Suffix
     *
     * @return $this
     */
    public function setNameSuffix($nameSuffix)
    {
        $this->offsetSet('nameSuffix', $nameSuffix);

        return $this;
    }
    /**
     * Gets username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->offsetGet('username');
    }

    /**
     * Sets username
     *
     * @param string $username Username
     *
     * @return $this
     */
    public function setUsername($username)
    {
        $this->offsetSet('username', $username);

        return $this;
    }
    /**
     * Gets description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->offsetGet('description');
    }

    /**
     * Sets description
     *
     * @param string $description Description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->offsetSet('description', $description);

        return $this;
    }
    /**
     * Gets position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->offsetGet('position');
    }

    /**
     * Sets position
     *
     * @param string $position Position
     *
     * @return $this
     */
    public function setPosition($position)
    {
        $this->offsetSet('position', $position);

        return $this;
    }
    /**
     * Gets department
     *
     * @return string
     */
    public function getDepartment()
    {
        return $this->offsetGet('department');
    }

    /**
     * Sets department
     *
     * @param string $department Department
     *
     * @return $this
     */
    public function setDepartment($department)
    {
        $this->offsetSet('department', $department);

        return $this;
    }
    /**
     * Gets status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->offsetGet('status');
    }

    /**
     * Sets status
     *
     * @param string $status Status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->offsetSet('status', $status);

        return $this;
    }
    /**
     * Gets isAdmin
     *
     * @return bool
     */
    public function getIsAdmin()
    {
        return $this->offsetGet('isAdmin');
    }

    /**
     * Sets isAdmin
     *
     * @param bool $isAdmin Is admin
     *
     * @return $this
     */
    public function setIsAdmin($isAdmin)
    {
        $this->offsetSet('isAdmin', $isAdmin);

        return $this;
    }
    /**
     * Gets email
     *
     * @return \Data2CRMAPI\Model\Email[]
     */
    public function getEmail()
    {
        return $this->offsetGet('email');
    }

    /**
     * Sets email
     *
     * @param \Data2CRMAPI\Model\Email[] $email Email
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $this->offsetSet('email', $email);

        return $this;
    }
    /**
     * Gets phone
     *
     * @return \Data2CRMAPI\Model\Phone[]
     */
    public function getPhone()
    {
        return $this->offsetGet('phone');
    }

    /**
     * Sets phone
     *
     * @param \Data2CRMAPI\Model\Phone[] $phone Phone
     *
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->offsetSet('phone', $phone);

        return $this;
    }
    /**
     * Gets messenger
     *
     * @return \Data2CRMAPI\Model\Messenger[]
     */
    public function getMessenger()
    {
        return $this->offsetGet('messenger');
    }

    /**
     * Sets messenger
     *
     * @param \Data2CRMAPI\Model\Messenger[] $messenger Messenger
     *
     * @return $this
     */
    public function setMessenger($messenger)
    {
        $this->offsetSet('messenger', $messenger);

        return $this;
    }
    /**
     * Gets website
     *
     * @return \Data2CRMAPI\Model\Website[]
     */
    public function getWebsite()
    {
        return $this->offsetGet('website');
    }

    /**
     * Sets website
     *
     * @param \Data2CRMAPI\Model\Website[] $website Website
     *
     * @return $this
     */
    public function setWebsite($website)
    {
        $this->offsetSet('website', $website);

        return $this;
    }
    /**
     * Gets address
     *
     * @return \Data2CRMAPI\Model\Address[]
     */
    public function getAddress()
    {
        return $this->offsetGet('address');
    }

    /**
     * Sets address
     *
     * @param \Data2CRMAPI\Model\Address[] $address Address
     *
     * @return $this
     */
    public function setAddress($address)
    {
        $this->offsetSet('address', $address);

        return $this;
    }
    /**
     * Gets createdBy
     *
     * @return \Data2CRMAPI\Model\UserEntityRelation
     */
    public function getCreatedBy()
    {
        return $this->offsetGet('createdBy');
    }

    /**
     * Sets createdBy
     *
     * @param \Data2CRMAPI\Model\UserEntityRelation $createdBy Created By
     *
     * @return $this
     */
    public function setCreatedBy($createdBy)
    {
        $this->offsetSet('createdBy', $createdBy);

        return $this;
    }
    /**
     * Gets createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->offsetGet('createdAt');
    }

    /**
     * Sets createdAt
     *
     * @param \DateTime $createdAt Created At
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->offsetSet('createdAt', $createdAt);

        return $this;
    }
    /**
     * Gets updatedBy
     *
     * @return \Data2CRMAPI\Model\UserEntityRelation
     */
    public function getUpdatedBy()
    {
        return $this->offsetGet('updatedBy');
    }

    /**
     * Sets updatedBy
     *
     * @param \Data2CRMAPI\Model\UserEntityRelation $updatedBy Updated By
     *
     * @return $this
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->offsetSet('updatedBy', $updatedBy);

        return $this;
    }
    /**
     * Gets updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->offsetGet('updatedAt');
    }

    /**
     * Sets updatedAt
     *
     * @param \DateTime $updatedAt Updated At
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->offsetSet('updatedAt', $updatedAt);

        return $this;
    }
}
