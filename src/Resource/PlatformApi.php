<?php

namespace Data2CRMAPI\Resource;

use Data2CRMAPI\Model\PlatformEntity;

class PlatformApi extends AbstractApi
{
    const HAS_QUERY_FILTER = false;
    const HAS_QUERY_FIELDS = true;
    
    /**
     * @var string
     */
    protected $path = '/platform';

    /**
     * @param null|int $pageSize
     * @param null|int $page
     * @param array $fields
     * 
     * @return PlatformEntity[]
     */
    public function fetchAll($pageSize = null, $page = null, array $fields = array())
    {
        return $this->doFetchAll($pageSize, $page, [], $fields, '\Data2CRMAPI\Model\PlatformEntity[]');
    }

    /**
     * @param string $type
     * 
     * @return PlatformEntity
     */
    public function fetch($type)
    {
        return $this->doFetch($type, '\Data2CRMAPI\Model\PlatformEntity');
    }
}
