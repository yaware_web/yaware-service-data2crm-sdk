<?php

namespace Data2CRMAPI\Resource;

use Data2CRMAPI\Model\NoteDescribe;
use Data2CRMAPI\Model\NoteEntity;
use Data2CRMAPI\Model\NoteEntityRelation;
use Data2CRMAPI\Model\Count;

class NoteApi extends AbstractApi
{
    const HAS_QUERY_FILTER = true;
    const HAS_QUERY_FIELDS = true;
    
    /**
     * @var string
     */
    protected $path = '/note';

    /**
     * @return NoteDescribe
     */
    public function describe()
    {
        return $this->doDescribe('\Data2CRMAPI\Model\NoteDescribe');
    }

    /**
     * @return Count
     */
    public function count()
    {
        return $this->doCount('\Data2CRMAPI\Model\Count');
    }

    /**
     * @param null|int $pageSize
     * @param null|int $page
     * @param array $filter
     * @param array $fields
     * 
     * @return NoteEntity[]
     */
    public function fetchAll($pageSize = null, $page = null, array $filter = array(), array $fields = array())
    {
        return $this->doFetchAll($pageSize, $page, $filter, $fields, '\Data2CRMAPI\Model\NoteEntity[]');
    }

    /**
     * @param string $id
     * 
     * @return NoteEntity
     */
    public function fetch($id)
    {
        return $this->doFetch($id, '\Data2CRMAPI\Model\NoteEntity');
    }

    /**
     * @param NoteEntity $note
     *
     * @return NoteEntityRelation
     */
    public function create(NoteEntity $note)
    {
        return $this->doCreate($note, '\Data2CRMAPI\Model\NoteEntityRelation');
    }

    /**
     * @param string $id
     * @param NoteEntity $note
     * 
     * @return NoteEntityRelation
     */
    public function update($id, NoteEntity $note)
    {
        return parent::doUpdate($id, $note, '\Data2CRMAPI\Model\NoteEntityRelation');
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        parent::doDelete($id);
    }
}
