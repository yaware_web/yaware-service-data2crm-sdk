<?php

namespace Data2CRMAPI;

class Configuration
{
    /**
     * Headers
     */
    const HEADER_API2CRM_USER_KEY           = 'X-API2CRM-USER-KEY';
    const HEADER_API2CRM_APPLICATION_KEY    = 'X-API2CRM-APPLICATION-KEY';

    /**
     * Default headers
     *
     * @var array
     */
    protected $defaultHeaders = array(
        'Accept' => 'application/json',
        'Content-Type' => 'application/json',
    );

    /**
     * The host
     *
     * @var string
     */
    protected $host = 'https://api-backend.api2crm.com/v1';

    /**
     * Timeout (second) of the HTTP request, by default set to 0, no timeout
     *
     * @var string
     */
    protected $timeout = 0;

    /**
     * User agent of the HTTP request
     *
     * @var string
     */
    protected $userAgent = "Data2CRM.API PHP SDK (https://www.data2crm.com/api/) /1.1.0";

    /**
     * Indicates if SSL verification should be enabled or disabled.
     *
     * This is useful if the host uses a self-signed SSL certificate.
     *
     * @var boolean True if the certificate should be validated, false otherwise.
     */
    protected $sslVerification = true;

    /**
     * Sets user key
     *
     * @param string $userKey user key
     *
     * @return Configuration
     */
    public function setUserKey($userKey)
    {
        return $this->addDefaultHeader(static::HEADER_API2CRM_USER_KEY, $userKey);
    }

    /**
     * Gets user key
     *
     * @return string
     */
    public function getUserKey()
    {
        return $this->getDefaultHeader(static::HEADER_API2CRM_USER_KEY);
    }

    /**
     * Sets Application key
     *
     * @param string $applicationKey application key
     *
     * @return Configuration
     */
    public function setApplicationKey($applicationKey)
    {
        return $this->addDefaultHeader(static::HEADER_API2CRM_APPLICATION_KEY, $applicationKey);
    }

    /**
     * Gets Application key
     *
     * @return string
     */
    public function getApplicationKey()
    {
        return $this->getDefaultHeader(static::HEADER_API2CRM_APPLICATION_KEY);
    }

    /**
     * Sets accept
     *
     * @param string $accept
     *
     * @return Configuration
     */
    public function setAccept($accept)
    {
        return $this->addDefaultHeader('Accept', $accept);
    }

    /**
     * Gets accept
     *
     * @return string
     */
    public function getAccept()
    {
        return $this->getDefaultHeader('Accept');
    }

    /**
     * Sets content type
     *
     * @param string $contentType
     *
     * @return Configuration
     */
    public function setContentType($contentType)
    {
        return $this->addDefaultHeader('Content-Type', $contentType);
    }

    /**
     * Gets content type
     *
     * @return string
     */
    public function getContentType()
    {
        return $this->getDefaultHeader('ContentType');
    }

    /**
     * Adds a default header
     *
     * @param string $headerName  header name (e.g. Token)
     * @param string $headerValue header value (e.g. 1z8wp3)
     *
     * @return ApiClient
     */
    public function addDefaultHeader($headerName, $headerValue)
    {
        if (!is_string($headerName)) {
            throw new \InvalidArgumentException('Header name must be a string.');
        }

        $this->defaultHeaders[$headerName] =  $headerValue;
        return $this;
    }

    /**
     * @param string $key
     * @param mixed $default
     * 
     * @return mixed
     */
    public function getDefaultHeader($key, $default = null)
    {
        return isset($this->defaultHeaders[$key]) ? $this->defaultHeaders[$key] : $default;
    }

    /**
     * Gets the default header
     *
     * @return array An array of default header(s)
     */
    public function getDefaultHeaders()
    {
        return $this->defaultHeaders;
    }

    /**
     * Deletes a default header
     *
     * @param string $headerName the header to delete
     *
     * @return Configuration
     */
    public function deleteDefaultHeader($headerName)
    {
        unset($this->defaultHeaders[$headerName]);
    }

    /**
     * Sets the host
     *
     * @param string $host Host
     *
     * @return Configuration
     */
    public function setHost($host)
    {
        $this->host = $host;
        return $this;
    }

    /**
     * Gets the host
     *
     * @return string Host
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * Sets the user agent of the api client
     *
     * @param string $userAgent the user agent of the api client
     *
     * @return ApiClient
     */
    public function setUserAgent($userAgent)
    {
        if (!is_string($userAgent)) {
            throw new \InvalidArgumentException('User-agent must be a string.');
        }

        $this->userAgent = $userAgent;
        return $this;
    }

    /**
     * Gets the user agent of the api client
     *
     * @return string user agent
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }

    /**
     * Sets the HTTP timeout value
     *
     * @param integer $seconds Number of seconds before timing out [set to 0 for no timeout]
     *
     * @return ApiClient
     */
    public function setTimeout($seconds)
    {
        if (!is_numeric($seconds) || $seconds < 0) {
            throw new \InvalidArgumentException('Timeout value must be numeric and a non-negative number.');
        }

        $this->timeout = $seconds;
        return $this;
    }

    /**
     * Gets the HTTP timeout value
     *
     * @return string HTTP timeout value
     */
    public function getTimeout()
    {
        return $this->timeout;
    }

    /**
     * Sets if SSL verification should be enabled or disabled
     *
     * @param boolean $sslVerification True if the certificate should be validated, false otherwise
     *
     * @return Configuration
     */
    public function setSSLVerification($sslVerification)
    {
        $this->sslVerification = $sslVerification;
        return $this;
    }

    /**
     * Gets if SSL verification should be enabled or disabled
     *
     * @return boolean True if the certificate should be validated, false otherwise
     */
    public function getSSLVerification()
    {
        return $this->sslVerification;
    }
}
