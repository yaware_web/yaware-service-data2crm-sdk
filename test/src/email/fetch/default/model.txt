Data2CRMAPI\Model\EmailEntity::__set_state(array(
   'container' => 
  array (
    'id' => '79648681',
    'direction' => NULL,
    'from' => 's.kulchytsky@magneticone.com',
    'to' => 'bill wall <mail12s@mail.com>',
    'cc' => NULL,
    'bcc' => NULL,
    'subject' => 'sssssss',
    'body' => 'sssssss',
    'status' => NULL,
    'sentAt' => 
    DateTime::__set_state(array(
       'date' => '2016-06-09 14:58:48',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
    'parent' => NULL,
    'relation' => 
    Data2CRMAPI\Model\Relation::__set_state(array(
       'container' => 
      array (
        'account' => 
        array (
          0 => 
          Data2CRMAPI\Model\AccountRelation::__set_state(array(
             'container' => 
            array (
              'type' => NULL,
              'entity' => 
              Data2CRMAPI\Model\AccountEntityRelation::__set_state(array(
                 'container' => 
                array (
                  'id' => '211373855',
                ),
                 'raw' => 
                array (
                  'id' => '211373855',
                ),
              )),
            ),
             'raw' => 
            array (
              'entity' => 
              array (
                'id' => '211373855',
              ),
              'type' => NULL,
            ),
          )),
        ),
        'contact' => 
        array (
          0 => 
          Data2CRMAPI\Model\ContactRelation::__set_state(array(
             'container' => 
            array (
              'type' => NULL,
              'entity' => 
              Data2CRMAPI\Model\ContactEntityRelation::__set_state(array(
                 'container' => 
                array (
                  'id' => '901',
                ),
                 'raw' => 
                array (
                  'id' => '901',
                ),
              )),
            ),
             'raw' => 
            array (
              'entity' => 
              array (
                'id' => '901',
              ),
              'type' => NULL,
            ),
          )),
        ),
        'lead' => 
        array (
        ),
        'opportunity' => 
        array (
        ),
        'task' => 
        array (
        ),
        'call' => 
        array (
        ),
        'email' => 
        array (
        ),
        'event' => 
        array (
        ),
        'meeting' => 
        array (
        ),
        'note' => 
        array (
        ),
        'attachment' => 
        array (
          0 => 
          Data2CRMAPI\Model\AttachmentRelation::__set_state(array(
             'container' => 
            array (
              'type' => NULL,
              'entity' => 
              Data2CRMAPI\Model\AttachmentEntityRelation::__set_state(array(
                 'container' => 
                array (
                  'id' => '4336687960',
                ),
                 'raw' => 
                array (
                  'id' => '4336687960',
                ),
              )),
            ),
             'raw' => 
            array (
              'entity' => 
              array (
                'id' => '4336687960',
              ),
              'type' => NULL,
            ),
          )),
          1 => 
          Data2CRMAPI\Model\AttachmentRelation::__set_state(array(
             'container' => 
            array (
              'type' => NULL,
              'entity' => 
              Data2CRMAPI\Model\AttachmentEntityRelation::__set_state(array(
                 'container' => 
                array (
                  'id' => '4181403177',
                ),
                 'raw' => 
                array (
                  'id' => '4181403177',
                ),
              )),
            ),
             'raw' => 
            array (
              'entity' => 
              array (
                'id' => '4181403177',
              ),
              'type' => NULL,
            ),
          )),
          2 => 
          Data2CRMAPI\Model\AttachmentRelation::__set_state(array(
             'container' => 
            array (
              'type' => NULL,
              'entity' => 
              Data2CRMAPI\Model\AttachmentEntityRelation::__set_state(array(
                 'container' => 
                array (
                  'id' => '4135222754',
                ),
                 'raw' => 
                array (
                  'id' => '4135222754',
                ),
              )),
            ),
             'raw' => 
            array (
              'entity' => 
              array (
                'id' => '4135222754',
              ),
              'type' => NULL,
            ),
          )),
          3 => 
          Data2CRMAPI\Model\AttachmentRelation::__set_state(array(
             'container' => 
            array (
              'type' => NULL,
              'entity' => 
              Data2CRMAPI\Model\AttachmentEntityRelation::__set_state(array(
                 'container' => 
                array (
                  'id' => '4190080136',
                ),
                 'raw' => 
                array (
                  'id' => '4190080136',
                ),
              )),
            ),
             'raw' => 
            array (
              'entity' => 
              array (
                'id' => '4190080136',
              ),
              'type' => NULL,
            ),
          )),
        ),
        'campaign' => 
        array (
        ),
        'project' => 
        array (
        ),
        'case' => 
        array (
        ),
      ),
       'raw' => 
      array (
        'account' => 
        array (
          0 => 
          array (
            'entity' => 
            array (
              'id' => '211373855',
            ),
            'type' => NULL,
          ),
        ),
        'contact' => 
        array (
          0 => 
          array (
            'entity' => 
            array (
              'id' => '901',
            ),
            'type' => NULL,
          ),
        ),
        'attachment' => 
        array (
          0 => 
          array (
            'entity' => 
            array (
              'id' => '4336687960',
            ),
            'type' => NULL,
          ),
          1 => 
          array (
            'entity' => 
            array (
              'id' => '4181403177',
            ),
            'type' => NULL,
          ),
          2 => 
          array (
            'entity' => 
            array (
              'id' => '4135222754',
            ),
            'type' => NULL,
          ),
          3 => 
          array (
            'entity' => 
            array (
              'id' => '4190080136',
            ),
            'type' => NULL,
          ),
        ),
        'lead' => 
        array (
        ),
        'opportunity' => 
        array (
        ),
        'task' => 
        array (
        ),
        'call' => 
        array (
        ),
        'email' => 
        array (
        ),
        'event' => 
        array (
        ),
        'meeting' => 
        array (
        ),
        'note' => 
        array (
        ),
        'campaign' => 
        array (
        ),
        'project' => 
        array (
        ),
        'case' => 
        array (
        ),
      ),
    )),
    'assignedUser' => NULL,
    'user' => 
    Data2CRMAPI\Model\UserEntityRelation::__set_state(array(
       'container' => 
      array (
        'id' => '2446043',
      ),
       'raw' => 
      array (
        'id' => '2446043',
      ),
    )),
    'createdBy' => 
    Data2CRMAPI\Model\UserEntityRelation::__set_state(array(
       'container' => 
      array (
        'id' => '2446043',
      ),
       'raw' => 
      array (
        'id' => '2446043',
      ),
    )),
    'createdAt' => 
    DateTime::__set_state(array(
       'date' => '2016-06-13 09:02:06',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
    'updatedBy' => 
    Data2CRMAPI\Model\UserEntityRelation::__set_state(array(
       'container' => 
      array (
        'id' => '2446043',
      ),
       'raw' => 
      array (
        'id' => '2446043',
      ),
    )),
    'updatedAt' => 
    DateTime::__set_state(array(
       'date' => '2016-06-13 09:02:06',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
   'raw' => 
  array (
    'id' => '79648681',
    'direction' => NULL,
    'from' => 's.kulchytsky@magneticone.com',
    'to' => 'bill wall <mail12s@mail.com>',
    'cc' => NULL,
    'bcc' => NULL,
    'subject' => 'sssssss',
    'body' => 'sssssss',
    'status' => NULL,
    'sent_at' => '2016-06-09T14:58:48+0000',
    'parent' => NULL,
    'relation' => 
    array (
      'account' => 
      array (
        0 => 
        array (
          'entity' => 
          array (
            'id' => '211373855',
          ),
          'type' => NULL,
        ),
      ),
      'contact' => 
      array (
        0 => 
        array (
          'entity' => 
          array (
            'id' => '901',
          ),
          'type' => NULL,
        ),
      ),
      'attachment' => 
      array (
        0 => 
        array (
          'entity' => 
          array (
            'id' => '4336687960',
          ),
          'type' => NULL,
        ),
        1 => 
        array (
          'entity' => 
          array (
            'id' => '4181403177',
          ),
          'type' => NULL,
        ),
        2 => 
        array (
          'entity' => 
          array (
            'id' => '4135222754',
          ),
          'type' => NULL,
        ),
        3 => 
        array (
          'entity' => 
          array (
            'id' => '4190080136',
          ),
          'type' => NULL,
        ),
      ),
      'lead' => 
      array (
      ),
      'opportunity' => 
      array (
      ),
      'task' => 
      array (
      ),
      'call' => 
      array (
      ),
      'email' => 
      array (
      ),
      'event' => 
      array (
      ),
      'meeting' => 
      array (
      ),
      'note' => 
      array (
      ),
      'campaign' => 
      array (
      ),
      'project' => 
      array (
      ),
      'case' => 
      array (
      ),
    ),
    'assigned_user' => NULL,
    'user' => 
    array (
      'id' => '2446043',
    ),
    'created_by' => 
    array (
      'id' => '2446043',
    ),
    'created_at' => '2016-06-13T09:02:06+0000',
    'updated_by' => 
    array (
      'id' => '2446043',
    ),
    'updated_at' => '2016-06-13T09:02:06+0000',
  ),
))