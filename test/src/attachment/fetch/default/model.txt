Data2CRMAPI\Model\AttachmentEntity::__set_state(array(
   'container' => 
  array (
    'id' => '00P58000000cdhYEAQ',
    'title' => NULL,
    'description' => 'desc',
    'name' => 'task',
    'mimeType' => NULL,
    'size' => 1105,
    'parent' => NULL,
    'relation' => 
    Data2CRMAPI\Model\Relation::__set_state(array(
       'container' => 
      array (
        'account' => 
        array (
        ),
        'contact' => 
        array (
        ),
        'lead' => 
        array (
        ),
        'opportunity' => 
        array (
        ),
        'task' => 
        array (
          0 => 
          Data2CRMAPI\Model\TaskRelation::__set_state(array(
             'container' => 
            array (
              'type' => 'Parent ID (ParentId)',
              'entity' => 
              Data2CRMAPI\Model\TaskEntityRelation::__set_state(array(
                 'container' => 
                array (
                  'id' => '00T58000004fLbeEAE',
                ),
                 'raw' => 
                array (
                  'id' => '00T58000004fLbeEAE',
                ),
              )),
            ),
             'raw' => 
            array (
              'type' => 'Parent ID (ParentId)',
              'entity' => 
              array (
                'id' => '00T58000004fLbeEAE',
              ),
            ),
          )),
        ),
        'call' => 
        array (
        ),
        'email' => 
        array (
        ),
        'event' => 
        array (
        ),
        'meeting' => 
        array (
        ),
        'note' => 
        array (
        ),
        'attachment' => 
        array (
        ),
        'campaign' => 
        array (
        ),
        'project' => 
        array (
        ),
        'case' => 
        array (
        ),
      ),
       'raw' => 
      array (
        'task' => 
        array (
          0 => 
          array (
            'type' => 'Parent ID (ParentId)',
            'entity' => 
            array (
              'id' => '00T58000004fLbeEAE',
            ),
          ),
        ),
        'account' => 
        array (
        ),
        'contact' => 
        array (
        ),
        'lead' => 
        array (
        ),
        'opportunity' => 
        array (
        ),
        'call' => 
        array (
        ),
        'email' => 
        array (
        ),
        'event' => 
        array (
        ),
        'meeting' => 
        array (
        ),
        'note' => 
        array (
        ),
        'attachment' => 
        array (
        ),
        'campaign' => 
        array (
        ),
        'project' => 
        array (
        ),
        'case' => 
        array (
        ),
      ),
    )),
    'assignedUser' => 
    Data2CRMAPI\Model\UserEntityRelation::__set_state(array(
       'container' => 
      array (
        'id' => '00558000000dmueAAA',
      ),
       'raw' => 
      array (
        'id' => '00558000000dmueAAA',
      ),
    )),
    'user' => 
    Data2CRMAPI\Model\UserEntityRelation::__set_state(array(
       'container' => 
      array (
        'id' => '00558000000dmueAAA',
      ),
       'raw' => 
      array (
        'id' => '00558000000dmueAAA',
      ),
    )),
    'createdBy' => 
    Data2CRMAPI\Model\UserEntityRelation::__set_state(array(
       'container' => 
      array (
        'id' => '00558000000dmueAAA',
      ),
       'raw' => 
      array (
        'id' => '00558000000dmueAAA',
      ),
    )),
    'createdAt' => 
    DateTime::__set_state(array(
       'date' => '2016-05-12 08:59:30',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
    'updatedBy' => 
    Data2CRMAPI\Model\UserEntityRelation::__set_state(array(
       'container' => 
      array (
        'id' => '00558000000dmueAAA',
      ),
       'raw' => 
      array (
        'id' => '00558000000dmueAAA',
      ),
    )),
    'updatedAt' => 
    DateTime::__set_state(array(
       'date' => '2016-05-12 13:40:46',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
   'raw' => 
  array (
    'id' => '00P58000000cdhYEAQ',
    'title' => NULL,
    'description' => 'desc',
    'link' => 'https://domain.com/tmp/file/00P58000000cdhYEAQ',
    'name' => 'task',
    'mime_type' => NULL,
    'size' => 1105,
    'parent' => NULL,
    'relation' => 
    array (
      'task' => 
      array (
        0 => 
        array (
          'type' => 'Parent ID (ParentId)',
          'entity' => 
          array (
            'id' => '00T58000004fLbeEAE',
          ),
        ),
      ),
      'account' => 
      array (
      ),
      'contact' => 
      array (
      ),
      'lead' => 
      array (
      ),
      'opportunity' => 
      array (
      ),
      'call' => 
      array (
      ),
      'email' => 
      array (
      ),
      'event' => 
      array (
      ),
      'meeting' => 
      array (
      ),
      'note' => 
      array (
      ),
      'attachment' => 
      array (
      ),
      'campaign' => 
      array (
      ),
      'project' => 
      array (
      ),
      'case' => 
      array (
      ),
    ),
    'assigned_user' => 
    array (
      'id' => '00558000000dmueAAA',
    ),
    'user' => 
    array (
      'id' => '00558000000dmueAAA',
    ),
    'created_by' => 
    array (
      'id' => '00558000000dmueAAA',
    ),
    'created_at' => '2016-05-12T08:59:30+0000',
    'updated_by' => 
    array (
      'id' => '00558000000dmueAAA',
    ),
    'updated_at' => '2016-05-12T13:40:46+0000',
  ),
))