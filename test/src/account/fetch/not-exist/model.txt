array (
  'message' => 'Bad Request',
  'code' => 400,
  'type' => 'http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html',
  'detailCode' => 'NOT_VALID',
  'detailMessage' => 'Request validation failed: one or more fields are not valid',
  'detailExtra' => 
  array (
  ),
)