<?php

namespace Data2CRMAPITest\src;

use Data2CRMAPI\ApiException;
use Data2CRMAPI\Data;
use Data2CRMAPI\Model\AbstractModel;
use Data2CRMAPI\ObjectSerializer;
use Data2CRMAPI\Resource\AbstractApi;
use \FilesystemIterator;

class AllTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Set up
     */
    public function setUp()
    {
        require_once __DIR__ . '/../bootstrap.php';
    }

    /**
     * @return array
     */
    public function providerAll()
    {
        $result = array();

        $resourcesIterator = new FilesystemIterator(__DIR__, FilesystemIterator::SKIP_DOTS);

        /** @var \SplFileInfo $resourceDir */
        foreach ($resourcesIterator as $resourceDir) {

            if (false === $resourceDir->isDir()) {
                continue;
            }

            $methodsIterator = new FilesystemIterator($resourceDir, FilesystemIterator::SKIP_DOTS);

            /** @var \SplFileInfo $methodDir */
            foreach ($methodsIterator as $methodDir) {

                if (false === $methodDir->isDir()) {
                    continue;
                }

                $methodTestIterator = new FilesystemIterator($methodDir, FilesystemIterator::SKIP_DOTS);

                /** @var \SplFileInfo $methodTestDir */
                foreach ($methodTestIterator as $methodTestDir) {

                    if (false === $methodTestDir->isDir()) {
                        continue;
                    }

                    $resourceName = $resourceDir->getFilename();
                    $methodName = $methodDir->getFilename();
                    $testName = $methodTestDir->getFilename();

                    $result[$resourceName . '::' . $methodName . ' #' . $testName] = [
                        '$dir' => $methodTestDir,
                        '$resource' => $resourceName,
                        '$method' => $methodDir->getFilename(),
                    ];
                }
            }
        }

        return $result;
    }

    /**
     * @dataProvider providerAll
     *
     * @param \SplFileInfo $dir
     * @param string $resource
     * @param string $method
     */
    public function testAll(\SplFileInfo $dir, $resource, $method)
    {
        $mock = json_decode(file_get_contents($dir->__toString() . '/mock.json'), true);

        $client = $this->getClient();
        $client->setCallMock($mock['call']['arguments'], $mock['call']['result']);

        $expected = str_replace(array("\r\n", "\r"), "\n", file_get_contents($dir->__toString() . '/model.txt'));

        $class = '\Data2CRMAPI\Resource\\' . ucfirst($resource) . 'Api';
        
        /** @var AbstractApi $api */
        $api = new $class($client);
        $api->setApiClient($client);
        $api->setData(new Data());
        
        $result = null;

        try {
            
            $suffix = '';
            if ('application' === $resource) {
                $suffix = 'Write';
            }
            
            $arguments = $mock['arguments'];
            $serializer = new ObjectSerializer();
            switch ($method) {
                
                case 'create':
                    $arguments[0] = $serializer->deserialize(
                        $arguments[0],
                        '\Data2CRMAPI\Model\\' . ucfirst($resource) . 'Entity' . $suffix
                    );
                    break;
                
                case 'update':
                    $arguments[1] = $serializer->deserialize(
                        $arguments[1],
                        '\Data2CRMAPI\Model\\' . ucfirst($resource) . 'Entity' . $suffix
                    );
                    break;
                
            }
            
            $result = call_user_func_array(array($api, $method), $arguments);
            
            $actual = var_export($result, true);

        } catch (ApiException $exception) {
            $actual = var_export(
                array(
                    'message' => $exception->getMessage(),
                    'code' => $exception->getCode(),
                    'type' => $exception->getType(),
                    'detailCode' => $exception->getDetailCode(),
                    'detailMessage' => $exception->getDetailMessage(),
                    'detailExtra' => $exception->getDetailExtra(),
                ),
                true
            );
        } catch (\Exception $exception) {
            $actual = var_export(
                array(
                    'message' => $exception->getMessage(),
                    'code' => $exception->getCode(),
                    'file' => $exception->getFile(),
                    'line' => $exception->getLine(),
                ),
                true
            );
        }

        $actual = str_replace(array("\r\n", "\r"), "\n", $actual);

        $this->assertEquals(
            $expected,
            $actual,
            'Not equal  (length of expected is ' . strlen($expected) . ', actual - ' . strlen($actual) . ')'
        );

        $this->assertEquals($mock['call']['result']['data'], serialize(call_user_func(array($api, 'getData'))));
        
        if ($result instanceof AbstractModel) {
            $this->callGetters($result);
        }
    }

    /**
     * @param AbstractModel $model
     */
    protected function callGetters(AbstractModel $model)
    {
        foreach ($model::getters() as $item) {
            $result = call_user_func(array($model, $item));
            if ($result instanceof AbstractModel) {
                $this->callGetters($result);
            } else if (true === is_array($result)) {
                foreach ($result as $arrayItem) {
                    if ($arrayItem instanceof AbstractModel) {
                        $this->callGetters($arrayItem);
                    }
                }
            }
        }
    }

    /**
     * @return ApiClient
     */
    protected function getClient()
    {
        $configuration = new \Data2CRMAPI\Configuration();
        $configuration->setUserKey('test-user-key');
        $configuration->setApplicationKey('test-application-key');

        return new ApiClient($configuration);
    }
}
